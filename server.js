const express = require('express');
const Joi = require('joi');
const bodyParser = require('body-parser');
const app = express();
const fs = require('fs');
app.use(express.json());
const routes = require('./routes/routes.js')(app, fs);

const server = app.listen(3001, () => {
  console.log('listening on port %s...', server.address().port);
});