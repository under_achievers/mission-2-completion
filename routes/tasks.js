const userRoutes = (app, fs) => {
    const tasks = [
        { id: 1, task: "Prepare for exam" },
        { id: 2, task: "Organize your table"},
        { id: 3, task: "book movie tickets"}
    ];

    
    //Display task
    app.get('/tasks', (req, res) => {
        res.send(tasks);
    
    })
     //Add task
    app.post('/tasks', (req, res) => {
    
        const task = {
            id: req.body.id, 
            task: req.body.task
        };
        tasks.push(task);
        res.send(tasks);
    });

    //Update task

    app.put('/tasks/:id', (req, res) => {
        const task = tasks.find(c => c.id == parseInt(req.params.id));
        if (!task) res.status(404).send(" Task not found...");
        task.id =req.body.id,
        task.task = req.body.task;
        res.send(tasks);
    
    })
    //Delete task
    
    app.delete('/tasks/:id', (req, res) => {
        const task = tasks.find(c => c.id == parseInt(req.params.id));
        if (!task) res.status(404).send("Task not found...");
        const index = tasks.indexOf(task);
        tasks.splice(index, 1);
        res.send(tasks);
    
    });
    
   
};

module.exports = userRoutes;
