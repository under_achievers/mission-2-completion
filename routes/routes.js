const userRoutes = require('./tasks');


const appRouter = (app, fs) => {
  
  app.get('/', (req, res) => {
    res.send('TODO LIST APP');
    
  });

  userRoutes(app, fs);
};
module.exports = appRouter;