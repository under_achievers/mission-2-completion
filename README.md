# Mission 2-Completion

A full Todo Rest API using Postman has been coded.

# ADDING A TASK
A new task is added using "POST" method in postman.

# UPDATE A TASK
You should enter the task id whose data to be updated.Update the details.
It is done by "PUT" method in postman.

# DELETE A TASK
You should enter the task id whose data to be deleted.
It is done by "DELETE" method in postman.

# DISPLAY ALL TASKS
It is done by "get" method in postman.

After cloning this repository install all the required packages.
